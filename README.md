# 韩先超老师微信公众号，定期发布技术类文章。
 

![输入图片说明](11111111.png)


# 韩老师k8s精品班课程报名地址
韩先超老师k8s高薪架构师课程CKA和CKS精品班正在招生中，精品班课程提供技术+简历+面试指导+岗位内推，采用直播+录播模式进行全方位讲解。精品班包含国企真实项，除了k8s技术外，还包含安全渗透、go开发k8s管理平台等。
精品班课程详细地址：https://edu.51cto.com/surl=VSmCT1



# k8s架构师课程CKA和CKS学习过程遇到的问题汇总

#### 介绍
51CTO k8s精品班学员学习过程会遇到一些问题，把经常遇到的问题统一放到这个仓库下面，方便大家排查。



#### 1.如果做k8s高可用安装实验，出现两台机器搭建keepalived，但是两台机器都能看到vip，这种情况是有问题的，如何排查解决？

答：

如，我的keepalive有两台机器，第一台是192.168.0.29，是master节点，第二台是192.168.0.186，是backup节点

在192.168.0.29机器，修改keepalived配置文件，添加如下参数


```
unicast_src_ip 192.168.0.29         ##source ip，当前keepalive机器的ip
    unicast_peer {
          192.168.0.186               ##dest ip，另一台keepalive机器的ip
}

```

在192.168.0.186机器，修改keepalived配置文件，添加如下参数


```
unicast_src_ip 192.168.0.186         ##source ip，当前keepalive机器的ip
    unicast_peer {
          192.168.0.29           ##dest ip，另一台keepalive机器的ip
}
```

![输入图片说明](1.kleepalived.png)

#### 2.kubeadm初始化k8s集群报错failed to parse kernel config：unable to load kernel module：”configs
答：
Kubeadm初始化k8s,会检测configs模块，configs模块现在centos操作系统不在维护了，可以忽略这个检查，那么kubeadm 初始化只需要加上参数--ignore-preflight-errors=SystemVerification即可

#### 3.写dockerfile文件时，如果dockerfile里基础镜像用的是FROM centos，然后构建镜像报错Failed to download metadata for repo ‘appstream’: Cannot prepare internal mirrorlist: No URLs in mirrorlist

答：如果FROM centos,那就是相当默认用的centos镜像版本是centos8，centos8自带的yum源目前已经无法使用了，需要单独创建个阿里云的源，具体步骤如下：
进到跟dockerfile文件同级目录
vim Centos-vault-8.5.2111.repo，写入如下内容：

```
# CentOS-Base.repo
#
# The mirror system uses the connecting IP address of the client and the
# update status of each mirror to pick mirrors that are updated to and
# geographically close to the client.  You should use this for CentOS updates
# unless you are manually picking other mirrors.
#
# If the mirrorlist= does not work for you, as a fall back you can try the 
# remarked out baseurl= line instead.
#
#
 
[base]
name=CentOS-8.5.2111 - Base - mirrors.aliyun.com
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/BaseOS/$basearch/os/
        http://mirrors.aliyuncs.com/centos-vault/8.5.2111/BaseOS/$basearch/os/
        http://mirrors.cloud.aliyuncs.com/centos-vault/8.5.2111/BaseOS/$basearch/os/
gpgcheck=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-Official
 
#additional packages that may be useful
[extras]
name=CentOS-8.5.2111 - Extras - mirrors.aliyun.com
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/extras/$basearch/os/
        http://mirrors.aliyuncs.com/centos-vault/8.5.2111/extras/$basearch/os/
        http://mirrors.cloud.aliyuncs.com/centos-vault/8.5.2111/extras/$basearch/os/
gpgcheck=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-Official
 
#additional packages that extend functionality of existing packages
[centosplus]
name=CentOS-8.5.2111 - Plus - mirrors.aliyun.com
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/centosplus/$basearch/os/
        http://mirrors.aliyuncs.com/centos-vault/8.5.2111/centosplus/$basearch/os/
        http://mirrors.cloud.aliyuncs.com/centos-vault/8.5.2111/centosplus/$basearch/os/
gpgcheck=0
enabled=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-Official
 
[PowerTools]
name=CentOS-8.5.2111 - PowerTools - mirrors.aliyun.com
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/PowerTools/$basearch/os/
        http://mirrors.aliyuncs.com/centos-vault/8.5.2111/PowerTools/$basearch/os/
        http://mirrors.cloud.aliyuncs.com/centos-vault/8.5.2111/PowerTools/$basearch/os/
gpgcheck=0
enabled=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-Official


[AppStream]
name=CentOS-8.5.2111 - AppStream - mirrors.aliyun.com
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/AppStream/$basearch/os/
        http://mirrors.aliyuncs.com/centos-vault/8.5.2111/AppStream/$basearch/os/
        http://mirrors.cloud.aliyuncs.com/centos-vault/8.5.2111/AppStream/$basearch/os/
gpgcheck=0
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-Official

```


然后在dockerfile文件里，需要把默认的yum源删除，把刚才新建的Centos-vault-8.5.2111.repo文件拷贝到镜像里
vim dockerfile

```
FROM centos
MAINTAINER xianchao
RUN rm -rf /etc/yum.repos.d/*
COPY Centos-vault-8.5.2111.repo /etc/yum.repos.d/
RUN yum install wget -y
RUN yum install nginx -y
COPY index.html /usr/share/nginx/html/
EXPOSE 80
ENTRYPOINT ["/usr/sbin/nginx","-g","daemon off;"]

```
#### 4.云主机安装k8s，如果calico不能正常运行，需要把如下图片相关端口放开
![输入图片说明](4-%E4%BA%91%E4%B8%BB%E6%9C%BA%E5%AE%89%E8%A3%85k8s.png)

#### 5.删除k8s名称空间，一直卡在terminating状态，如何解决?
![输入图片说明](5-%E5%88%A0%E9%99%A4%E5%90%8D%E7%A7%B0%E7%A9%BA%E9%97%B4.png)


#### 6.kubeadm init初始化k8s之后，没成功，想要重新初始化，但是如果初始化过，在重新初始化就会报错，显示端口被占用，如何解决？
答：

```
在节点执行kubeadm reset即可，这个命令是重置k8s集群，会清空k8s所有组件和资源。
```

#### 7.containerd拉取镜像，需要指定镜像完整名称，跟docker拉取镜像有区别，那用containerd命令行工具ctr如何拉取镜像呢？

[root@xianchaonode1 ~]# ctr images pull busybox:1.28

报错如下：
`ctr: failed to resolve reference "busybox:1.28": parse "dummy://busybox:1.28": invalid port ":1.28" after host`

上面报错说明，containerd如果作为容器运行时，用ctr拉取镜像，需要写镜像完整的路径才可以，如下：
`[root@xianchaonode1 ~]# ctr images pull docker.io/library/busybox:1.28`

#### 8. K8s从1.24版本开始，已经废弃docker了，但是不代表不需要学docker，因为我们还得用dockerfile做镜像。但是如果k8s版本用的是1.24或者比1.24还新的版本

K8s1.24之前的版本用的容器运行时大都是是docker，K8s1.24之后，容器运行时用的大部分都是containerd，那么containerd跟docker有什么区别呢？
主要是导入镜像的方式不同。
K8s1.24之前，用docker load –i 指定镜像压缩文件导出镜像可以被pod找到
K8s1.24和1.24之后版本，用ctr –n=k8s.io images import  指定镜像压缩文件导出镜像，才可以被pod识别到

#### 9.calico问题，如果自己做实验的机器有多个网卡，需要修改配置calico.yaml文件，指定具体使用哪个网卡接口，如果自己机器网卡接口是ens33，按照如下配置

修改calico.yaml配置文件：
增加如下两行：

```
- name: IP_AUTODETECTION_METHOD
  value: "interface=ens33" 
```


注意：interface=ens33，ens33是物理机网卡接口，大家要以自己实际的网卡接口为主。这个网卡必须要能正常访问网络才可以

![输入图片说明](9-calico.png)

修改配置之后，重新安装calico：

```
kubectl delete -f  calico.yaml
kubectl apply  -f  calico.yaml
```

#### 10.jenkins构建失败，salve pod显示offline
具体报错如下：
![输入图片说明](11-jenkins%20slave.jpg)

一、在做cka第三模块实验时，jenkins构建的时候提示offline

排查思路：

1）jenkins更新速度快，但是如果jenkins slave pod需要的镜像也要随着jenkins更新，所以为了大家做实验顺利，我们统一jenkins版本，用2.421，其他版本会出现slave连不上主jenkins：

jenkins-deployment.yaml文件里的镜像用如下版本：
image:  jenkins/jenkins:2.421

备注：上述镜像如果自己拉取速度慢，可以从网盘把镜像压缩包jenkins2.421.tar.gz下载到自己k8s工作节点，网盘地址如下：

```
链接：https://pan.baidu.com/s/1UzgrMpi4jj-VpPRIba28DA 
提取码：CHAO 

```


在k8s工作节点手动解压镜像：

1）如果是containerd做容器运行时，用如下命令解压

```
[root@xianchaonode1 ]# ctr -n=k8s.io images import jenkins2.421.tar.gz

```

2）如果是docker做容器运行时，用如下命令解压

```
[root@xianchaonode1 ]# docker load -i jenkins2.421.tar.gz

```


jenkins slave pod镜像用：

jenkins-slave-latest:v1


二、在做cks第三模块实验时，jenkins构建的时候提示offline
排查思路：

1）jenkins更新速度快，但是如果jenkins slave pod需要的镜像也要随着jenkins更新，所以为了大家做实验顺利，我们统一jenkins版本，用2.421，其他版本会出现slave连不上主jenkins：

jenkins-deployment.yaml文件里的镜像用如下版本：
image:  jenkins/jenkins:2.421

备注：上述镜像如果自己拉取速度慢，可以从网盘把镜像压缩包jenkins2.421.tar.gz下载到自己k8s工作节点，网盘地址如下：

```
链接：https://pan.baidu.com/s/1UzgrMpi4jj-VpPRIba28DA 
提取码：CHAO 

```


在k8s工作节点手动解压镜像：
1）如果是containerd做容器运行时，用如下命令解压

```
[root@xianchaonode1 ]# ctr -n=k8s.io images import jenkins2.421.tar.gz

```

2）如果是docker做容器运行时，用如下命令解压

```
[root@xianchaonode1 ]# docker load -i jenkins2.421.tar.gz

```

三、如果jenkins用的是jenkins/jenkins:2.421,那就会出现，好多插件在线安装失败，那可以按照如下方法离线安装：

1、访问jenkins主页面，找到当前jenkins版本要下载的镜像：
`http://192.168.40.180:30002/manage/pluginManager/available`

假如安装gitlab api插件，在可选插件搜索gitlab api
![输入图片说明](gitlabapi.png)

可以看到当前jenkins对应的gitlab api版本是5.3.0-91.v1f9a_fda_d654f

2、接下来访问清华地址，找到这个版本镜像：
http://mirrors.tuna.tsinghua.edu.cn/jenkins/plugins/

![输入图片说明](gitlab-qinghua.png)

进到gitlab-api目录下
![输入图片说明](590.png)

选择5.3.0-91.v1f9a_fda_d654f版本下载即可

![输入图片说明](hpi.png)

下载的是hpi结尾的文件

3、手动上传刚才下载的离线文件

回到jenkins主页，找到插件管理-Advanced settings
![输入图片说明](ad.png)


找到部署插件，选择文件
![输入图片说明](%E6%96%87%E4%BB%B6.png)

选择刚才下载的hpi结尾的文件即可

![输入图片说明](%E9%83%A8%E7%BD%B2hpi.png)

点击部署
![输入图片说明](%E7%82%B9%E5%87%BB%E9%83%A8%E7%BD%B2.png)

出现下面表示安装完成，点击安装完后重启Jenkins，这样就重新加载jenkins，插件就生效了
![输入图片说明](%E7%94%9F%E6%95%88.png)


#### 11 cka第三模块efk搭建，nfs-provisioner供应商镜像说明

第三模块efk实验，搭建nfs-provisioner，需要把deployment.yaml文件的镜像替换，替换成如下镜像，就不需要修改apiserver配置文件了。

registry.cn-beijing.aliyuncs.com/mydlq/nfs-subdir-external-provisioner:v4.0.0

![输入图片说明](nfs-prov.png)

替换之后重新安装，

```
kubectl delete -f deployment.yaml
kubectl apply -f deployment.yaml
```
#### 12.二进制安装k8s报错，排查思路

[root@xianchaonode1 ~]# systemctl start kubelet

[root@xianchaomaster1 work]# kubectl get csr


```
正常会出现下面的信息：
NAME                                                   AGE   SIGNERNAME                                    REQUESTOR           CONDITION
node-csr-SY6gROGEmH0qVZhMVhJKKWN3UaWkKKQzV8dopoIO9Uc   87s   kubernetes.io/kube-apiserver-client-kubelet   kubelet-bootstrap   Pending
```


但是执行kubectl approve签发的时候，kubectl get nodes看不到节点

原因分析：

可以先看看kubelet日志，一般日志可能报错信息不会太明显
systemctl status kubelet -l

看看前面步骤：

1、配置文件格式乱码：所有的配置文件我在课件里都有，你从课件里传到自己的机器上
2、token.csv文件可能有问题

#创建token.csv文件

```
[root@xianchaomaster1 ~]# cd /data/work/

```

```
[root@xianchaomaster1 work]# cat > token.csv << EOF
$(head -c 16 /dev/urandom | od -An -t x | tr -d ' '),kubelet-bootstrap,10001,"system:kubelet-bootstrap"
EOF
```

`#格式：token，用户名，UID，用户组`

token.csv格式如下:    
`3940fd7fbb391d1b4d861ad17a1f0613,kubelet-bootstrap,10001,"system:kubelet-bootstrap"`

`3、[root@xianchaomaster1 work]# BOOTSTRAP_TOKEN=$(awk -F "," '{print $1}' /etc/kubernetes/token.csv)`

`4、echo $BOOTSTRAP_TOKEN`
，$BOOTSTRAP_TOKEN这个变量对应的值要能看到，是3940fd7fbb391d1b4d861ad17a1f0613才可以
